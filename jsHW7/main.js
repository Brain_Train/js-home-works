// // 1

const searchCandidatesByPhoneNumber = phone => {
   let number = phone.match(/\d/g).join('')
   let result = condidateArr.filter(candidate=>candidate.phone.match(/\d/g).join('').indexOf(number)>=0);
   return result;
}
console.log(searchCandidatesByPhoneNumber('6-2'))


// const searchCandidatesByPhoneNumber = phone => {
//    const parsePhone = phones => {
//       let res = '';
//       for (let str of phones) {
//          if (isNaN(+str) || str === ' ') continue;
//          res += str
//       }
//       return res
//    }

//    return condidateArr.filter( item => {
//       const parseInPhone = parsePhone(phone)
//       const parsedCandPhone = parsePhone(item.phone)
//       return parsedCandPhone.includes(parseInPhone)
//    })
// }
// console.log(searchCandidatesByPhoneNumber());

// class Condidate {
//    constructor(phone) {
//       Object.assign(this, phone)
//    }
// }
// const searchCandidatesByPhoneNumber = (phone) => {
//    return condidateArr.filter(item => item.phone.includes(phone))
//    .map(cond => new Condidate(cond))
// }
// console.log(searchCandidatesByPhoneNumber('6-2'))

// // 2
const getCandidateById = (id) => {
   let candidate = condidateArr.find((item) => item._id === id);
   candidate.registered = candidate.registered
      .substring(0, candidate.registered.search('T'))
      .split('-')
      .join('/');
   return candidate
}
console.log(getCandidateById('5e216bc9fec2d80d5a9e96c7'));
 

// // 3

class Condidate {
   constructor(data){
      Object.assign(this, data)
   }
   get balanceAsNum() {
      return +this.balance.replace(/\D/g, '')
   }
}

const newArr = condidateArr.map(cond => new Condidate(cond));

const sortCandidatesArr = sortby => {
   if (!sortby) return newArr;
   return [...newArr].sort((a, b) => {
      return 'asc' === sortby ? a.balanceAsNum - b.balanceAsNum : b.balanceAsNum - a.balanceAsNum;
   })
}
console.log(sortCandidatesArr('desk'));

// let balance = condidateArr.map (item => {
//    return {balance: +item.balance.replace(/[$,]/g, '')}

// })
// const sortCandidatesArr = sortby => {
//    if (!sortby) return balance;
//    return [...balance].sort((a, b) => {
//       return 'asc' === sortby ? a.balance - b.balance : b.balance - a.balance;
//    })
// }
// console.log(sortCandidatesArr('desk'));
 
//  // 4

const getEyeColorMap = () => {
   return condidateArr.reduce((acc, item) => {
      return {
         ...acc, 
         [item.eyeColor]: item.eyeColor in acc ? [...acc[item.eyeColor], item] : [item]
      }
   }, {})
}
console.log(getEyeColorMap());

// const getEyeColorMap = () => {
//    let getEyeColor = {}
//    condidateArr.forEach(item => !getEyeColor.hasOwnProperty(item.eyeColor) ? (getEyeColor[item.eyeColor] = [],
//       getEyeColor[item.eyeColor].push(item)) : getEyeColor[item.eyeColor].push(item))
//    return getEyeColor
// }
// console.log(getEyeColorMap());