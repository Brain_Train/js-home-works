const counter = () => {
  let start = 0;
  return (next) => {
    start += next;
    return start;
  }
}
const summary = counter();

const arr = () => {
  let start = []; 
  return (a) => {
    (a === undefined) ? start = [] : start.push(a), console.log(start)
  }
}
const getUpdatedArr = arr()
// тоже самое что и ^^^^^^^ но if else
// const arr = () => {
//   let start = [];
//   return (a) => {
//     if (a === undefined) {
//         return start = [];
//     }
//     else {
//       return start.push(a),
//       console.log(start);
//     }
//   }
// }
// const getUpdatedArr = arr();

const time = () => {
  let currentTime = new Date().getTime() ;
  return () => {
    const  raz = new Date().getTime() - currentTime;
    currentTime = new Date().getTime();
    return ~~(raz / 1000)
  } 
}
const ok = time();

const timer = (time) => {
  let currentTime = time + 1;
  const timerInterval = setInterval(() => {
    currentTime--;
    if (currentTime > 0) {
      const second = (sec) => {
        if (sec <= 60 && sec >= 10) {
          return (`${sec}`);
        }
        else if (sec < 10) {
          return (`0${sec}`);
        }
      }
      const minutes = () => {
        let minute = ~~(currentTime / 60);
        if (currentTime >= 60 && currentTime < 600) {
          return console.log(`0${minute}:${second(currentTime - (minute * 60))}`);
        }
        else if (currentTime >= 600 && currentTime < 3600) {
          return console.log(`${minute}:${second(currentTime - (minute * 60))}`);
        }
      }
      if (currentTime >= 60 && currentTime < 3600) {
      minutes();
      }
      else if (currentTime < 60) {
        return console.log(`00:${second(currentTime)}`);
      }
      else if (currentTime > 3600) {
        clearInterval(timerInterval);
        return console.log('установи время меньше часа(3600 sec)');
      }
    }
    else {
      clearInterval(timerInterval);
      return console.log('Kaboooom');
    }
  },1000)
}
timer(3605);