const emplyeeAsrr = [
   {
       id: 1,
       name: 'Денис',
       surname: 'Хрущ',
       salary: 1010, 
       workExperience: 10, 
       isPrivileges: false, 
       gender: 'male'
   },
   {
       id: 2,
       name: 'Сергей',
       surname: 'Войлов',
       salary: 1200, 
       workExperience: 12, 
       isPrivileges: false, 
       gender: 'male'
   },
   {
       id: 3,
       name: 'Татьяна',
       surname: 'Коваленко',
       salary: 480, 
       workExperience: 3, 
       isPrivileges: true, 
       gender: 'female'
   },
   {
       id: 4,
       name: 'Анна',
       surname: 'Кугир',
       salary: 2430, 
       workExperience: 20, 
       isPrivileges: false, 
       gender: 'female'
   },
   {
       id: 5,
       name: 'Татьяна',
       surname: 'Капустник',
       salary: 3150, 
       workExperience: 30, 
       isPrivileges: true, 
       gender: 'female'
   },
   {
       id: 6,
       name: 'Станислав',
       surname: 'Щелоков',
       salary: 1730, 
       workExperience: 15,
       isPrivileges: false, 
       gender: 'male'
   },
   {
       id: 7,
       name: 'Денис',
       surname: 'Марченко',
       salary: 5730, 
       workExperience: 45, 
       isPrivileges: true,
       gender: 'male'
   },
   {
       id: 8,
       name: 'Максим',
       surname: 'Меженский',
       salary: 4190, 
       workExperience: 39, 
       isPrivileges: false, 
       gender: 'male'
   },
   {
       id: 9,
       name: 'Антон',
       surname: 'Завадский',
       salary: 790, 
       workExperience: 7, 
       isPrivileges: false, 
       gender: 'male'
   },
   {
       id: 10,
       name: 'Инна',
       surname: 'Скакунова',
       salary: 5260, 
       workExperience: 49, 
       isPrivileges: true, 
       gender: 'female'
   },
   {
       id: 11,
       name: 'Игорь',
       surname: 'Куштым',
       salary: 300, 
       workExperience: 1, 
       isPrivileges: false, 
       gender: 'male'
   },
];

function Emploee (person) {
    this.id = person.id;
    this.name = person.name;
    this.surname = person.surname;
    this.salary = person.salary;
    this.workExperience = person.workExperience;
    this.isPrivileges = person.isPrivileges
    this.gender = person.gender
   
}

Emploee.prototype.getFullName = function () {
    return this.name + ' ' + this.surname;
}

Emploee.prototype.getAvgSalary = function () {
    return this.salary;
}

let createEmployesFromArr = () => {
    return Array.from(emplyeeAsrr);
}

let emplyeeConstructArr = [];
let avgSalary = [];

for (let key of createEmployesFromArr()) {
    emplyeeConstructArr.push(new Emploee(key).getFullName());
}
console.log(emplyeeConstructArr)

for (let key of createEmployesFromArr()) {
    avgSalary.push(new Emploee(key).getAvgSalary());
}
let sum = 0;
let raz = avgSalary.length;
for (let key of avgSalary) {
    sum += key;
}
let avg = Math.floor(sum / raz);
console.log("avg salary = " + avg);