const studentArr = [{
   name: 'Сергей',
   surname: 'Войлов',
   ratingPoint: 1000,
   schoolPoint: 1000,
   course: 2,
},
{
   name: 'Татьяна',
   surname: 'Коваленко',
   ratingPoint: 880,
   schoolPoint: 700,
   course: 1,
},
{
   name: 'Анна',
   surname: 'Кугир',
   ratingPoint: 1430,
   schoolPoint: 1200,
   course: 3,
},
{
   name: 'Станислав',
   surname: 'Щелоков',
   ratingPoint: 1130,
   schoolPoint: 1060,
   course: 2,
},
{
   name: 'Денис',
   surname: 'Хрущ',
   ratingPoint: 1000,
   schoolPoint: 1390,
   course: 4,
},
{
   name: 'Татьяна',
   surname: 'Капустник',
   ratingPoint: 650,
   schoolPoint: 500,
   course: 3,
},
{
   name: 'Максим',
   surname: 'Меженский',
   ratingPoint: 990,
   schoolPoint: 1100,
   course: 1,
},
{
   name: 'Денис',
   surname: 'Марченко',
   ratingPoint: 570,
   schoolPoint: 1300,
   course: 4,
},
{
   name: 'Антон',
   surname: 'Завадский',
   ratingPoint: 1090,
   schoolPoint: 1010,
   course: 3
},
{
   name: 'Игорь',
   surname: 'Куштым',
   ratingPoint: 870,
   schoolPoint: 790,
   course: 1,
},
{
   name: 'Инна',
   surname: 'Скакунова',
   ratingPoint: 1560,
   schoolPoint: 200,
   course: 2,
},
];

class Student {
   constructor(enrollee){
   this.id = Student.id++;
   this.name = enrollee.name;
   this.surname = enrollee.surname;
   this.ratingPoint = enrollee.ratingPoint;
   this.schoolPoint = enrollee.schoolPoint;
   this.isSelfPayment = true;
   }
   static id = 1;
}
let createStudents = () => {
   return Array.from(studentArr);
}
let listOfStudents = [];
for (let key of createStudents()) {
   listOfStudents.push(new Student(key));
}

let sortByRatingPoint = listOfStudents.sort((a, b) =>{
   if (a.ratingPoint == b.ratingPoint) {
       return b.schoolPoint - a.schoolPoint;
   } else {
       return b.ratingPoint - a.ratingPoint;
   }
});
let minimalSort = sortByRatingPoint.filter((a) => (a.ratingPoint >= 800));
let paimantFalsGuys = minimalSort.slice(0,5);
for(let i = 0; i < sortByRatingPoint.length; i++){
   for(let j = 0; j < paimantFalsGuys.length; j++){
         if (sortByRatingPoint[i].id == paimantFalsGuys[j].id){
            sortByRatingPoint[i].isSelfPayment = false;
         }
   }
}
// sortByRatingPoint.forEach(function(a){
//    return paimantFalsGuys.forEach(function(b) {
//       if (a.id === b.id)
//       return a.isSelfPayment = false;
//    });
// });                 тоже рабочий вариант вместо for выше
console.log(sortByRatingPoint);
 
class CustomString {
   constructor(reverse, ucFirst, ucWords) {
      this.reverse = reverse;
      this.ucFirst = ucFirst;
      this.ucWords = ucWords;
   }
   getReverse() {
      return this.reverse.split('').reverse().join('');
   }
   getUcFirst() {
      return [this.ucFirst].map(letter => letter.charAt(0).toUpperCase() + letter.slice(1)).join('' ); 
   }
   getUcWords() {
      return this.ucWords.split(' ').map(letter => letter.charAt(0).toUpperCase() + letter.slice(1)).join(' ');
   }
}

let custom = new CustomString('qwerty', 'qwerty', 'qwerty qwerty qwerty');
console.log(custom.getReverse());
console.log(custom.getUcFirst());
console.log(custom.getUcWords());

class Validator {
   constructor(){};
   getEmail(eamail) {
      return eamail.includes('@gmail.com');
   }
   getDomain(domain) {
      return domain.includes('.com');
   }
   getDate(date) {
      let chislo = date.split('.');
      if (Number.isNaN(Date.parse(chislo[2] + '-' + chislo[1] + '-' + chislo[0]))) {
         return false;
      }
      return true;
   }
   getPhone(phone) {
      phone.trim();
      let newStrPhone = '';
      for (let key of phone) {
         if (key === ' ' || key === ')' || key === '(' || key === '+' || key === '-') {
            continue;
         }
         newStrPhone += key;
      }
      newStrPhone = (newStrPhone.startsWith('380') && newStrPhone.length === 12) ? true : false;
      return newStrPhone;
   }
}
let vali = new Validator();
console.log(vali.getEmail('vasya.pupkin@gmail.com'));
console.log(vali.getDomain('google.com'));
console.log(vali.getDate('30.11.2019'));
console.log(vali.getPhone('+38 (066) 937-99-92'));